import os

from setuptools import setup

files = []
for r, d, f in os.walk("chad_launcher/data"):
    for file in f:
        if file.endswith(".scss") or file.endswith(".map") or file.endswith("~"):
            continue
        files.append(os.path.relpath(os.path.join(r, file), "chad_launcher/"))
setup(
    name="chad_launcher",
    version="0.1",
    packages=["chad_launcher", "chad_launcher.widgets"],
    package_dir={"chad_launcher": "./chad_launcher"},
    package_data={"chad_launcher": files},
    entry_points={
        "console_scripts": [
            "chad_launcher = chad_launcher.bootstrap:main",
        ],
    },
    install_requires=[
        "pyfzf",
    ],
)
