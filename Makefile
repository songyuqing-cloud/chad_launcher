build:
	python -m build

install:
	./install.sh

uninstall:
	./uninstall.sh

run:
	python -m chad_launcher

clean:
	rm -rf chad_launcher.egg-info/ dist/
