# chad launcher

A game launcher/P2P game store written in Python powered by the [GNU/Linux P2P Pirates](https://matrix.to/#/!SlYhhmreXjJylcsjfn:tedomum.net?via=matrix.org&via=tchncs.de) community on Matrix.

This is our idea on how to compete against Steam. We are fixing a lot of games but don't have a medium for providing them in a 'nice' interface. This a simple launcher and game store that will save information about games and run/configure start scripts with additional features our community might find nice to have such as gamepad support, detection of missing packages, protection against data gathering like telemetry and so on. 

We steered away from our initial plans to make this a tool for debugging games besides being a game store. We believe that the community shouldn't rely on GUI for debugging and repeat the problem that Lutris has created where people don't know how to navigate the terminal and only know how to run wine through it. This state of knowldedge is self-limiting and we want to avoid it.                                            

# Dependencies

- python3
- PyGObject
- gtk-3.0

# Build Dependencies

- pkg-config
- python-pip
- python-setuptools

# Installation

## Arch and Derivatives

### Aur Helper

#### Pamac
`pamac install chad_launcher-git`
#### Yay
`yay -Sy chad_launcher-git`
#### Paru
`paru -Sy chad_launcher-git`
### Manually
```shell
$ mkdir chad_launcher
$ cd chad_launcher
$ git clone https://aur.archlinux.org/chad_launcher-git.git
$ cd chad_launcher-git
$ makepkg -si
```

## Other Distros

```shell
$ mkdir chad_launcher
$ cd chad_launcher
$ git clone https://gitlab.com/Gnurur/chad_launcher.git
$ cd chad_launcher
$ sudo make install
```



# Config

## the config file should be location in `~/.config/chad_launcher/`. it should be called `config.yaml`

- `paths`: an array containing root paths to game library
- `proxy`: proxy to use for discover section. format: `[protocol]://[hostname]:[port]`
- `vars`: a list of variables to use to configure the gui.

## Example

### config.yaml

```yaml
paths:
	- $HOME/Games
	- $HOME/.local/share/games
proxy: https://localhost:8080
vars:
    terminal: alacritty
```

# Directory structure for `path` in `paths`

```
root_dir
 |- game1
     |- start.sh
 |- game2
     |- start.sh
```

- Every game dir should have a `start.sh` file which will be executed to launch the game.

# Screenshot of the current GUI
<div align="center"><img src="https://i.postimg.cc/qRTN2z5T/34234.jpg"></div>
