import asyncio
import logging
import subprocess
import time
from typing import List, Optional

from fuzzywuzzy import process
from gi.repository import GLib, Gtk

from . import config, indexer
from .controller import Controller
from .discover_game import DiscoverGame
from .widgets.discover import DiscoverLibrary
from .fetcher.game_fetcher import GameFetcher
from .fetcher.leetx_fetcher import LeetxFetcher

SCROLL_BOTTOM_THRESHOLD = 10

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class DiscoverController(Controller):
    view: DiscoverLibrary
    search: bool
    fetcher: GameFetcher
    selected_game: Optional[DiscoverGame]
    shown: bool

    def __init__(self, view):
        super().__init__()

        self.view = view
        self.num_games = 0
        self.fetcher = LeetxFetcher()
        self.search = False
        self.selected_game = None
        self.shown = False

        self.view.setup()

        self.connect_handler(self.view.register_magnet_handler, self.handle_download)
        self.connect_handler(self.view.register_import_handler, self.handle_import)
        self.connect_handler(
            self.view.game_grid.register_select_game_handler, self.handle_select_game
        )
        self.connect_handler(self.view.register_reload_handler, self.handle_reload)
        self.connect_handler(
            self.view.game_grid.register_scroll_handler, self.handle_scroll
        )

    async def handle_scroll(self, value, upper, page_size):
        # Disable loading new games when in search mode
        if self.search:
            return

        if value > upper - page_size - SCROLL_BOTTOM_THRESHOLD:
            GLib.idle_add(self.view.set_loading, True)
            games = await self.fetcher.get_more_games()
            await self.update_games_grid(games)
            GLib.idle_add(self.view.set_loading, False)

    def handle_show(self):
        if not self.shown:
            self.shown = True
            self.run_async(self.handle_reload)

    async def handle_download(self):
        game = self.selected_game
        if game is None:
            logger.warn("no game selected")
            return
        subprocess.Popen(["xdg-open", game.magnet])

    async def handle_select_game(self, game):
        GLib.idle_add(self.view.update_panel, game)
        self.selected_game = game

    async def handle_reload(self):
        GLib.idle_add(self.view.set_loading, True)
        self.fetcher.reset()
        games = await self.fetcher.get_more_games()
        await self.update_games_grid(games, reload=True)
        GLib.idle_add(self.view.set_loading, False)

    async def handle_search(self, query):
        GLib.idle_add(self.view.set_loading, True)

        if query == "":
            self.search = False
            self.fetcher.reset()
            games = await self.fetcher.get_more_games()
        else:
            self.search = True
            games = await self.fetcher.search_games(query)

        await self.update_games_grid(games, reload=True)
        GLib.idle_add(self.view.set_loading, False)

    async def update_games_grid(self, games: List[DiscoverGame], reload=False):
        GLib.idle_add(self.view.update_games, games, not reload)

        if len(games) > 0 and self.selected_game is None:
            await self.handle_select_game(games[0])

    async def handle_import(self):
        terminal = config.get_var("terminal")
        paths = config.parse_config()["paths"]
        dlg = Gtk.Dialog()
        dlg.add_button("Import", Gtk.ResponseType.ACCEPT)
        dlg.add_button("Cancel", Gtk.ResponseType.CANCEL)
        box = dlg.get_content_area()
        checkboxes = []
        if not paths:
            logger.warn("set atleast one game directory to import games into")
            return
        if not terminal:
            logger.warn("no terminal set")
            return

        def parse_name(name):
            name = name.replace(".tar.zst", "")
            name = name.lower().replace("-", " ")
            name = name.lower().replace("_", " ")
            name = name.lower().replace(".", " ")
            return name.title()

        already_installed = [
            parse_name(game.name) for game in asyncio.run(indexer.get_games(paths))
        ]
        for path in paths:
            games = list(path.glob("*.tar.zst"))
            games.extend(path.glob("*/*.tar.zst"))

            for game in games:
                game = game.resolve()

                match = process.extractOne(
                    parse_name(game.name),
                    already_installed,
                )
                if not match or match[1] < 90:
                    logger.info(f"importing {game.name} to {path}")
                    checkbox = Gtk.CheckButton.new_with_label(parse_name(game.name))
                    checkbox.game = game
                    box.pack_start(checkbox, False, True, 2)
                    checkboxes.append(checkbox)

        box.show_all()
        response = dlg.run()
        if response == Gtk.ResponseType.CANCEL:
            dlg.destroy()
            return

        dlg.destroy()

        async def thread(checkboxes):
            for checkbox in checkboxes:
                if checkbox.get_active():
                    proc = subprocess.Popen(
                        [
                            terminal,
                            "-e",
                            "tar",
                            "-I",
                            "zstd",
                            "-xvf",
                            str(checkbox.game),
                        ],
                        cwd=paths[0],
                    )
                    while proc.poll() is None:
                        time.sleep(1)

        asyncio.create_task(thread(checkboxes))
