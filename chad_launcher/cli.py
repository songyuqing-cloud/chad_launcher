import asyncio
from typing import Optional

import typer
from pyfzf.pyfzf import FzfPrompt

from . import config, indexer

cli = typer.Typer()


@cli.command()
def list(type: str = typer.Argument(..., help="What to list. Possible values: games")):
    """List games."""
    if type == "games":
        for game in asyncio.run(indexer.get_games(config.parse_config()["paths"])):
            print(game.name)


@cli.command()
def run(
    name: Optional[str] = typer.Argument(None, help="Name of the game to launch."),
    interactive: bool = typer.Option(False, help="Run in interactive mode with fzf."),
):
    """Run a game."""
    games = asyncio.run(indexer.get_games(config.parse_config()["paths"]))
    if interactive:
        fzf = FzfPrompt()
        name = fzf.prompt([game.name for game in games])[0]
        for game in games:
            if game.name == name:
                game.launch_game()
                raise typer.Exit()

    if name is None:
        typer.echo("[warning] Not running in interactive mode and no game name passed.")
        raise typer.Exit()

    for game in games:
        if game.name == name:
            game.launch_game()
            raise typer.Exit()


if __name__ == "__main__":
    cli()
