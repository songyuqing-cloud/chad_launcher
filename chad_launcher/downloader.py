import logging
import re
from pathlib import Path
from subprocess import PIPE, STDOUT, Popen, check_output

from . import config
from .utils import which

logger = logging.getLogger(__name__)


class Download:
    def __init__(self, magnet, path: Path):
        self.magnet = magnet
        self.path = path
        if not self.path.is_dir():
            self.path.mkdir(parents=True)
        self.parse_args()
        self.status = "idle"
        self.percent_done = 0

    def parse_args(self):
        self.args = []
        self.args.append("--follow-torrent=mem")
        self.args.append("--enable-color=false")
        self.args.append("--summary-interval=0")
        self.args.append("--bt-save-metadata=true")
        self.args.append("--bt-load-saved-metadata=true")
        if upload_limit := config.get_var("upload-limit"):
            self.args.append(f"--max-upload-limit={upload_limit}")
        if download_limit := config.get_var("download-limit"):
            self.args.append(f"--max-download-limit={download_limit}")

    @property
    def progress(self):
        if self.status == "started":
            regex = re.compile(r"(\d+[KMGT]?i?B)/(\d+[KMGT]?i?B)")
            line = self.proc.stdout.readline().strip()
            if line != "":
                logger.debug(f"aria2:{line}")
            if match := regex.search(line):
                done, total = match.groups()
                size_map = {
                    "B": 1,
                    "KiB": 1024,
                    "MiB": pow(1024, 2),
                    "GiB": pow(1024, 3),
                    "TiB": pow(1024, 4),
                }
                regex = re.compile(r"\d+")
                units = regex.sub("", done)
                done = regex.match(done)
                if not done:
                    return self.percent_done
                done = int(done.group(0)) * size_map[units]
                units = regex.sub("", total)
                total = regex.match(total)
                if not total:
                    return self.percent_done
                total = int(total.group(0)) * size_map[units]
                if total == 0:
                    return self.percent_done
                self.percent_done = done / total
            return self.percent_done

    @property
    def percent_done(self):
        return self._percent_done

    @percent_done.setter
    def percent_done(self, value):
        if not 0 <= value <= 1:
            raise ValueError("percent_done can only be between 0 and 1")
        if not (isinstance(value, float) or isinstance(value, int)):
            raise ValueError("percent_done needs to be of type float or int")
        self._percent_done = value

    @property
    def proc(self):
        return self._proc

    @proc.setter
    def proc(self, value):
        if not isinstance(value, Popen):
            raise TypeError("download process must be of Popen Type.")
        self._proc = value

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, value):
        statuses = ("idle", "started", "paused", "stopped", "finished", "error")
        if value not in statuses:
            raise ValueError("status can only be one of", statuses)
        self._status = value

    def start(self):
        if not (aria2c := which("aria2c")):
            logger.warn("install aria2c for automatic downloads")
            return

        self.proc = Popen(
            [aria2c, *self.args, self.magnet],
            bufsize=1,
            stdout=PIPE,
            stderr=STDOUT,
            text=True,
            cwd=self.path,
        )
        self.status = "started"
        logger.info("started download successfully")

    def finish(self):
        self.status = "finished"
        logger.info("download successful")

    def error(self):
        self.status = "error"
        logger.info("download failed")


class Extractor:
    def __init__(self, filepath: Path, path: Path):
        self.path = path
        if not self.path.is_dir():
            self.path.mkdir(parents=True)
        self.filepath = filepath
        self.parse_args()
        self.status = "idle"
        self.percent_done = 0
        self.members = ()

    def parse_args(self):
        self.args = []
        self.args.extend(["-I", "zstd", "-xvf"])

    @property
    def progress(self):
        if self.status == "started":
            current_member = self.proc.stdout.readline().strip()
            logger.debug(f"tar:{current_member}")
            if current_member in self.members:
                self.percent_done = self.members.index(current_member) / len(
                    self.members
                )
            return self.percent_done

    @property
    def members(self):
        tar = which("tar")
        if not (tar and which("zstd")):
            logger.warn("install tar and zstd for automatic extraction.")
            return ()
        if not len(self._members):
            self.members = (
                check_output([tar, "tf", self.filepath]).decode().strip().split("\n")
            )
        return self._members

    @members.setter
    def members(self, value):
        self._members = value

    @property
    def percent_done(self):
        return self._percent_done

    @percent_done.setter
    def percent_done(self, value):
        if not 0 <= value <= 1:
            raise ValueError("percent_done can only be between 0 and 1")
        if not (isinstance(value, float) or isinstance(value, int)):
            raise ValueError("percent_done needs to be of type float or int")
        self._percent_done = value

    @property
    def proc(self):
        return self._proc

    @proc.setter
    def proc(self, value):
        if not isinstance(value, Popen):
            raise TypeError("download process must be of Popen Type.")
        self._proc = value

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, value):
        statuses = ("idle", "started", "paused", "stopped", "finished", "error")
        if value not in statuses:
            raise ValueError("status can only be one of", statuses)
        self._status = value

    def start(self):
        tar = which("tar")
        if not (tar and which("zstd")):
            logger.warn("install tar and zstd for automatic extraction")
            return
        self.proc = Popen(
            [tar, *self.args, self.filepath],
            bufsize=1,
            stdout=PIPE,
            stderr=STDOUT,
            text=True,
            cwd=self.path,
        )
        self.status = "started"
        logger.info("started extraction successfully")

    def finish(self):
        self.status = "finished"
        logger.info("extraction successful")

    def error(self):
        self.status = "error"
        logger.info("extraction failed")


def move(file, path):
    if not path.is_file():
        raise FileNotFoundError(file)
    if not path.is_dir():
        path.mkdir()
