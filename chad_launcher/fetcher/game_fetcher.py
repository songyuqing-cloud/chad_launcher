

from ..discover_game import DiscoverGame
from typing import List

class GameFetcher:
    def reset(self):
        """ Reset internal state of the fetcher """
        pass

    async def get_more_games(self) -> List[DiscoverGame]:
        """ 
        Fetch more games. The length of the returned list is not defined, 
        but should be around the size of for example the size of a page 
        """
        return []

    async def search_games(self, query: str) -> List[DiscoverGame]:
        """ Search for games """
        return []
