
import asyncio
import logging
import aiohttp
import re
from bs4 import BeautifulSoup
from typing import List

from .. import config, indexer
from ..discover_game import DiscoverGame
from .game_fetcher import GameFetcher

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

BASE_URL = "https://1337x.to"
USER = "johncena141"
CONFIGS = config.parse_config()

GAMES_PER_PAGE = 20

class LeetxFetcher(GameFetcher):
    page_no: int    

    def __init__(self) -> None:
        super().__init__()    
        self.page_no = 1

    def reset(self):
        self.page_no = 1

    async def get_more_games(self) -> List[DiscoverGame]:
        """ 
        Fetch more games. The length of the returned list is not defined, 
        but should be around the size of for example the size of a page.
        """
        games = await self.get_games_on_page(self.page_no)
        self.page_no += 1
        return games

    async def search_games(self, query: str) -> List[DiscoverGame]:
        """ Search for games """
        url = f"{BASE_URL}/search/{query}+gnu+linux/1/"

        return await self.fetch_games(url)

    async def get_games_on_page(self, page_no: int) -> List[DiscoverGame]:
        url = f"{BASE_URL}/{USER}-torrents/{page_no}/"

        return await self.fetch_games(url)

    async def fetch_games(self, url) -> List[DiscoverGame]:
        session = aiohttp.ClientSession()
        games: List[DiscoverGame] = []
        proxy = config.get_var("proxy")
        if proxy:
            async with session.get(url, proxy=proxy) as resp:
                html = await resp.read()
        else:
            async with session.get(url) as resp:
                html = await resp.read()

        # parse the html
        soup = BeautifulSoup(html, "lxml")

        # find the table with list of torrents
        table = soup.find("table")
        if not table:
            return []
        table = table.tbody

        # loop over every row in the table and store torrent info in a dictionary
        for row in table.find_all("tr"):
            cols = row.find_all("td")

            link = BASE_URL + cols[0].find_all("a")[1].get("href").strip()
            title = cols[0].get_text().strip()
            seeds = cols[1].get_text().strip()
            leechers = cols[2].get_text().strip()
            size = cols[3].get_text()[:-1].strip()
            match = re.search(r".*\/(\d*)\/.*", link)
            if match:
                game_id = match.group(1).strip()
            else:
                game_id = "Unknown"

            # append the dictionary to the games list
            games.append(DiscoverGame(title, game_id, link, seeds, leechers, size))
            logger.debug(
                f"created game object for {title} with id {game_id} and link: {link}"
            )

        logger.debug("getting game info")
        await asyncio.gather(*(game.setup(session) for game in games))
        await session.close()

        return [
            game
            for game in games
            if game.version not in ("collection",)
        ]
