import logging
import os
import sys

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))


def main():
    if len(sys.argv) == 1:
        from . import app

        app.main()
    else:
        from . import cli

        cli.cli()
