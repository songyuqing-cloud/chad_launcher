import subprocess

from gi.repository import Gtk


def which(name: str):
    """
    Returns the path of the binary file whose name matches the paramater given.
    :param name [str]: [Name of the binary whose path needs to be found]
    """
    process = subprocess.run(
        ["which", name], stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    if process.returncode == 0:
        return process.stdout.decode("utf-8").strip()
    else:
        return False


def set_bg_image_css(widget, node, img_path):
    props = {"background-image": f'url("{img_path}")', "background-size": "cover"}
    set_css_props(widget, node, props)


def set_css_props(widget: Gtk.Widget, node: str, props: dict):
    prop_string = ""
    for prop, value in props.items():
        prop_string += f"{prop}: {value};\n"
    css = f"""
        {node} {{
            {prop_string}
        }}
        """.encode()
    set_css(widget, css)


def set_css(widget: Gtk.Widget, css: bytes):
    css_provider = Gtk.CssProvider()
    css_provider.load_from_data(css)
    style_context: Gtk.StyleContext = widget.get_style_context()
    style_context.add_provider(css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
