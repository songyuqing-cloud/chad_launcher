import asyncio
import logging
from pathlib import Path
from typing import List, Tuple

import aiohttp

from .library_game import LibraryGame

logger = logging.getLogger(__name__)


def get_game_dirs(root_paths: List[Path]) -> Tuple[Path, ...]:
    game_dirs: List[Path] = []

    # traverses every root_path in the root_paths var
    for root_path in root_paths:
        ignore = []
        ignore_file = root_path / ".chadignore"
        if ignore_file.is_file():
            logger.info("found .chadignore file")
            with ignore_file.open() as f:
                for line in f.readlines():
                    ignore.append(line.strip())

        for node in root_path.iterdir():
            if node in ignore:
                logger.debug(
                    f"ignoring directory {node} because its in .chadignore file."
                )
                continue
            node_path = node.resolve()

            # skip node if not directory
            if node_path.is_dir() and (node_path / "start.sh").is_file():
                logger.debug(f"found game {node_path}")
                game_dirs.append(node_path)
            elif node_path.is_dir() and (node_path / "start").is_file():
                logger.debug(f"found game {node_path}")
                game_dirs.append(node_path)

    return tuple(game_dirs)


async def get_games(root_paths: List[Path], force=False) -> Tuple[LibraryGame, ...]:
    games: List[LibraryGame] = []
    game_dirs: Tuple[Path, ...] = get_game_dirs(root_paths)
    session = aiohttp.ClientSession()

    for game_dir in game_dirs:
        games.append(LibraryGame(game_dir))

    await asyncio.gather(*(game.setup(session, force=force) for game in games))
    await session.close()
    if not len(games):
        logger.warning("No games found")

    return tuple(games)
