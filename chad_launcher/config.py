import logging
from os import environ, path
from pathlib import Path
from typing import Any, Dict

import yaml

logger = logging.getLogger(__name__)


PARSED_CONFIG = False
CONFIGS = {}


def get_config_path() -> Path:
    config_path = Path("~/.config/chad_launcher").expanduser()
    if "XDG_CONFIG_DIR" in environ:
        config_path = Path(path.expandvars("$XDG_CONFIG_DIR/chad_launcher"))
    return config_path


def get_var(var: str):
    vars = parse_config()["vars"]
    return vars.get(var)


def parse_config(force=False) -> Dict[str, Any]:
    global PARSED_CONFIG, CONFIGS
    if PARSED_CONFIG == False or force:
        configs = {}
        # set config path
        config_path = get_config_path()
        if not path.isdir(config_path):
            config_path.mkdir()

        # try to find a config.yaml
        if (config_path / "config.yaml").is_file():
            logger.info("found config.yaml")
            with (config_path / "config.yaml").open(mode="r") as f:
                configs = yaml.load(f, Loader=yaml.SafeLoader)
                for i, _path in enumerate(configs["paths"]):
                    configs["paths"][i] = Path(path.expandvars(_path))

        if "paths" not in configs:
            logger.info(f"found no paths in {config_path / 'config.yaml'}")
            configs["paths"] = []
        if "vars" not in configs:
            logger.info(f"found no vars in {config_path / 'config.yaml'}")
            configs["vars"] = {
                "upload-limit": "",
                "download-limit": "",
                "proxy": "",
                "terminal": "",
            }
        CONFIGS = configs
        PARSED_CONFIG = True
    return CONFIGS


def set_config(configs: dict):
    # set config path
    config_path = get_config_path()
    if not config_path.is_dir():
        config_path.mkdir()

    # try to find a config.yaml
    with (config_path / "config.yaml").open(mode="w") as f:
        for i, _path in enumerate(configs["paths"]):
            configs["paths"][i] = str(_path)
        yaml.dump(configs, f, Dumper=yaml.SafeDumper)
