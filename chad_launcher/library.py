import asyncio
import logging
import re
import string
import subprocess
from datetime import datetime
from typing import Optional

from gi.repository import GLib, Gtk

from . import config, indexer, utils
from .controller import Controller
from .library_game import LibraryGame
from .widgets.library import Library

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class LibraryController(Controller):
    view: Library
    updating: bool
    selected_game: Optional[LibraryGame]

    def __init__(self, view: Library) -> None:
        super().__init__()

        self.view = view
        self.updating = False
        self.selected_game = None

        self.view.setup()

        self.connect_handler(
            self.view.game_grid.register_select_game_handler, self.handle_select_game
        )
        self.connect_handler(self.view.register_terminal_handler, self.handle_terminal)
        self.connect_handler(
            self.view.register_refresh_data_handler, self.handle_refresh
        )
        self.connect_handler(self.view.register_launch_handler, self.handle_launch)

    def handle_show(self):
        self.run_async(self.update_games_grid)

    async def handle_search_changed(self, query: str):
        GLib.idle_add(self.view.set_query, query)

    async def handle_select_game(self, game):
        GLib.idle_add(self.view.update_panel, game)
        self.selected_game = game

    async def update_games_grid(self, force=False):
        GLib.idle_add(self.view.set_loading, True)

        if self.updating:
            logger.info("Updating store games.")
            return
        self.updating = True

        # get games
        root_paths = config.parse_config()["paths"]
        games = [game for game in await indexer.get_games(root_paths, force=force)]
        GLib.idle_add(self.view.update_games, games)

        if len(games) > 0 and self.selected_game is None:
            await self.handle_select_game(games[0])

        GLib.idle_add(self.view.set_loading, False)
        self.updating = False

    async def handle_terminal(self):
        if self.selected_game is None:
            logger.warn("no game selected")
            return

        configs = config.parse_config()
        _vars: dict = configs["vars"]
        if "terminal" not in _vars:
            return
        terminal: str = _vars["terminal"]
        subprocess.Popen(terminal, cwd=self.selected_game.root_path)

    async def handle_refresh(self):
        await self.update_games_grid(force=True)

    async def handle_launch(self):
        if self.selected_game is None:
            logger.warn("no game selected")
            return

        game = self.selected_game
        proc = game.launch_game()

        async def update_info(proc):
            # Do not access self in this thread, that would not be thread-safe!

            GLib.idle_add(self.view.set_game_log, "")
            logfile = game.root_path / "chad.log"
            if logfile.is_file():
                reached_limit = False
                with logfile.open(mode="r") as f:
                    lines = len(f.read().split("\n"))
                    if lines >= 5000:
                        reached_limit = True
                if reached_limit:
                    logfile.unlink()

            with logfile.open(mode="a") as f:
                f.write("==========================================================\n")
                f.write(datetime.today().strftime("%Y-%m-%d %T\n"))
                for line in iter(proc.stdout.readline, ""):
                    printable = set(string.printable)
                    regex = re.compile(r"(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]")
                    line = regex.sub("", line)
                    text = "".join(filter(lambda x: x in printable, line)).strip()
                    if not text:
                        continue
                    text = text + "\n"
                    f.write(text)

                    GLib.idle_add(self.view.append_to_game_log, text)
                f.write("==========================================================\n")

        asyncio.create_task(update_info(proc))
