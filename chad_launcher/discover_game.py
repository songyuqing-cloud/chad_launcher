import asyncio
import logging
import re
import string
from datetime import datetime
from pathlib import Path

import aiofiles
from bs4 import BeautifulSoup
from fuzzywuzzy import process

from . import config
from .game import Game

logger = logging.getLogger(__name__)

BANNER_LIST_URL = (
    "https://gitlab.com/chad-productions/chad_launcher_banners/-/raw/master/list.txt"
)

# TODO: (blackCauldron7): make info fetching asynchronous.
class DiscoverGame(Game):
    def __init__(self, title, id, link, seeds, leechers, size):
        self.title = title
        self.id = id
        self.link = link
        self.seeds = seeds
        self.leechers = leechers
        self.size = size
        self.data = False
        self.have_info = False
        self.banner_path = Path(
            f"~/.cache/chad_launcher/{self.id}-banner.png"
        ).expanduser()
        self.downloading = False
        self.parse_info()

    async def setup(self, session, force=False):
        await asyncio.gather(
            self.get_info(session, force=force), self.get_banner(session, force=force)
        )

    def parse_info(self):
        regex = re.compile(r"v?\d+[\.\w]*")
        self.name = self.title
        if "-" in self.name:
            self.name = self.name[: self.name.index("-")].strip()
        elif "[" in self.name:
            self.name = self.title[: self.title.index("[")].strip()
            # remove version from name
            self.name = regex.sub("", self.name)

        # get version
        match = regex.search(self.title)
        if "collection" in self.name.lower():
            self.version = "collection"
        elif "+" in self.name:
            self.version = "multi"
        else:
            if match is not None:
                self.version = match.group(0)
            else:
                self.version = "original"

        for char in self.name:
            if not char.isalnum():
                self.name.replace(char, "")
        # get type
        self.type = "native"
        if "wine" in self.title.lower():
            self.type = "wine"

    async def get_info(self, session, force=False):
        if self.have_info:
            return

        # get the html
        try:
            proxy = config.get_var("proxy")
            if proxy:
                async with session.get(self.link, proxy=proxy) as resp:
                    html = await resp.read()
            else:
                async with session.get(self.link) as resp:
                    html = await resp.read()
        except Exception as e:
            logger.debug(e)
            logger.warn("could not connect to 1337x.to")
            return

        # parse the html and grab image url and find game info.
        soup = BeautifulSoup(html, "lxml")
        for br in soup.find_all("br"):
            br.replace_with("\n")

        # get game info.
        infos = soup.find("div", attrs={"id": "description"}).get_text().strip()
        infos = [x.strip() for x in infos.split("\n\n")][:-3]

        # parse game description and sys reqs
        self.description = sys_reqs_str = ""
        for i, info in enumerate(infos):
            if info.lower().startswith("description"):
                self.description = "\n".join(
                    "\n\n".join(infos[i:]).split("\n")[1:]
                ).strip()
                printable = set(string.printable)
                self.description = "".join(
                    filter(lambda x: x in printable, self.description)
                )
            if info.lower().startswith("system requirements"):
                sys_reqs_str = "\n".join(infos[i].split("\n")[1:]).strip()

        # convert sys reqs to dictionary
        self.sys_reqs = {}
        for line in sys_reqs_str.split("\n"):
            if ":" in line:
                key, value = line.split(":", 1)
            else:
                key = "Additional Info"
                value = line
            self.sys_reqs[key.strip()] = value.strip()

        self.magnet: str = soup.find("a", href=re.compile("magnet")).get("href")

        self.have_info = True
        logger.debug(f"parsing info successful for {self.name}")

    async def get_banner(self, session, force=False):
        # get game image
        if not self.banner_path.is_file() or force:
            # get banner image
            banner_list = Path("~/.cache/chad_launcher/list.txt").expanduser()
            if not banner_list.parent.is_dir():
                banner_list.parent.mkdir()

            if not banner_list.is_file() or force:
                async with session.get(BANNER_LIST_URL) as resp:
                    list = await resp.read()
                    list = list.decode()

                banner_list.write_text(datetime.today().strftime("%d-%m-%Y\n") + list)
            else:
                async with aiofiles.open(banner_list, mode="r+") as f:
                    content = await f.read()
                    content = content.strip().split("\n")
                    if len(content) > 0:
                        list_date = datetime.strptime(content[0], "%d-%m-%Y").date()
                        if datetime.today().date() > list_date:
                            async with session.get(BANNER_LIST_URL) as resp:
                                list = await resp.read()
                                await f.write(datetime.today().strftime("%d-%m-%Y\n"))
                                await f.write(list.decode())
            banners = ()
            async with aiofiles.open(banner_list, mode="r") as f:
                banners = await f.read()
                banners = banners.split("\n")[1:]
                _banners: dict = {}
                for banner in banners:
                    if ":" in banner:
                        banner = banner.split(":")
                        _banners[banner[0].strip()] = banner[1].strip()
                    else:
                        _banners[banner.strip()] = banner.strip()
                banner_name = process.extractOne(self.name, _banners.keys())

                if banner_name is None:
                    return

                if banner_name[1] < 88:
                    return

                banner_name = _banners[banner_name[0]]

            banner_url = f"https://gitlab.com/chad-productions/chad_launcher_banners/-/raw/master/{banner_name}.png?inline=false"
            # get image.
            async with session.get(banner_url) as resp:
                async with aiofiles.open(self.banner_path, mode="wb") as f:
                    await f.write(await resp.read())
        logger.debug(f"fetched banner for {self.name}")
