import logging
import os.path
from pathlib import Path
import sys

import gi

gi.require_version("Gtk", "3.0")
gi.require_version("Gdk", "3.0")

from gi.repository import Gdk, Gio, Gtk

from . import config
from .settings import SettingsController
from .discover import DiscoverController
from .library import LibraryController
from .widgets.discover import DiscoverLibrary
from .widgets.header import HeaderBar
from .widgets.library import Library
from .widgets.settings import Settings

# Gtk.threads_init()


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class MyWindow(Gtk.ApplicationWindow):
    def __init__(self):
        logger.debug("init")
        Gtk.ApplicationWindow.__init__(self, title="Chad Launcher")
        self.set_css()
        self.start_threads()
        self.setup()

    def setup(self):
        self.set_default_size(1600, 900)

        self.headerbar = HeaderBar()
        self.set_titlebar(self.headerbar)

        self.stack = Gtk.Stack()
        # self.stack.set_transition_type(Gtk.StackTransitionType.SLIDE_RIGHT)
        self.headerbar.stack_switcher.set_stack(self.stack)
        self.headerbar.register_open_settings_handler(self.open_settings)
        self.headerbar.register_search_handler(self.handle_search)
        self.headerbar.register_search_changed_handler(self.handle_search_changed)

        self.stack.library = Library()
        self.library_controller = LibraryController(self.stack.library)
        self.stack.add_titled(self.stack.library, "library", "Library")
        self.library_controller.handle_show()

        self.stack.discover_library = DiscoverLibrary()
        self.discover_controller = DiscoverController(self.stack.discover_library)
        self.stack.add_titled(
            self.stack.discover_library, "discover_library", "Discover"
        )

        stacks = self.headerbar.stack_switcher.get_children()

        stacks[1].connect("clicked", lambda _: self.discover_controller.handle_show())
        self.add(self.stack)

    def handle_search(self, query: str):
        name = self.stack.get_visible_child_name()

        if name == "discover_library":
            self.discover_controller.run_async(self.discover_controller.handle_search, query)

    def handle_search_changed(self, query: str):
        name = self.stack.get_visible_child_name()

        if name == "library":
            self.library_controller.run_async(self.library_controller.handle_search_changed, query)
        
    def open_settings(self):
        settings = Settings()
        settings.connect("destroy", lambda _: settings.close)
        settings.show_all()
        self.settings_controller = SettingsController(settings)


    def set_css(self):
        config_path = config.get_config_path()
        css_path = os.path.join(os.path.dirname(__file__), "./data/styles/css/main.css")
        if os.path.exists(os.path.join(config_path, "main.css")):
            css_path = os.path.join(config_path, "main.css")
        css_provider = Gtk.CssProvider()
        css_file = Gio.File.new_for_path(css_path)
        css_provider.load_from_file(css_file)
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION,
        )

    def start_threads(self):
        pass
        # x = threading.Thread(target=discover.setup)
        # x.start()
        # self.threads.append(x)


def main():
    win = MyWindow()
    win.show_all()

    def close(win):
        Gtk.main_quit(win)

    win.connect("destroy", close)
    try:
        Gtk.main()
        return 0
    except Exception as e:
        logger.error(e)
        return 1


if __name__ == "__main__":
    try:
        sys.exit(main())
    except KeyboardInterrupt:
        sys.exit(127)
