import asyncio
from concurrent.futures.thread import ThreadPoolExecutor


class Controller:
    executor: ThreadPoolExecutor

    def __init__(self) -> None:
        self.executor = ThreadPoolExecutor(max_workers=1)

    def run_async(self, func, *args, **kwargs):
        self.executor.submit(asyncio.run, func(*args, **kwargs))

    def connect_handler(self, register_func, handler):
        register_func(lambda *args, **kwargs: self.run_async(handler, *args, **kwargs))
