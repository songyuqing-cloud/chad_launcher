import logging
from pathlib import Path

from gi.repository import GLib, Gtk

from .controller import Controller
from .widgets.settings import Settings
from . import config

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class SettingsController(Controller):
    view: Settings

    def __init__(self, view):
        super().__init__()
        self.view = view

        # TODO: Make handlers async. Needed data shouold be passed from VarList through handler.
        # Changes should be run in Gtk thread using GLib.idle_add. For now it all runs in the Gtk thread.
        self.view.paths_list.register_add_handler(self.add_path)
        self.view.paths_list.register_delete_handler(self.remove_path)
        self.view.paths_list.register_edit_handler(self.edit_path)

        self.view.vars_list.register_add_handler(self.add_var)
        self.view.vars_list.register_delete_handler(self.remove_var)
        self.view.vars_list.register_edit_handler(self.edit_var)

    def add_path(self):
        file_filter = Gtk.FileFilter()
        file_filter.add_mime_type("inode/directory")
        file_filter.set_name("Directory")
        dlg = Gtk.FileChooserDialog(
            "Open..",
            None,
            Gtk.FileChooserAction.SELECT_FOLDER,
            (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN,
                Gtk.ResponseType.ACCEPT,
            ),
        )
        dlg.add_filter(file_filter)

        dlg.run()
        dirname = dlg.get_filename()
        if dirname is None:
            dlg.destroy()
            return

        self.view.paths_list.add_var([dirname])
        configs = config.parse_config()
        configs["paths"].append(dirname)
        config.set_config(configs)
        dlg.destroy()


    def remove_path(self):
        dirname = self.view.paths_list.get_selected_value(0)
        configs = config.parse_config()
        configs["paths"].remove(Path(dirname))
        config.set_config(configs)
        self.view.paths_list.remove_selected()

    def edit_path(self):
        dirname = self.view.paths_list.get_selected_value(0)

        # get new dirname
        file_filter = Gtk.FileFilter()
        file_filter.add_mime_type("inode/directory")
        file_filter.set_name("Directory")
        dlg = Gtk.FileChooserDialog(
            "Open..",
            None,
            Gtk.FileChooserAction.SELECT_FOLDER,
            (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN,
                Gtk.ResponseType.ACCEPT,
            ),
        )
        dlg.add_filter(file_filter)

        response = dlg.run()
        if response == Gtk.ResponseType.CANCEL:
            dlg.destroy()
            return

        new_dirname = dlg.get_filename()
        if new_dirname is None:
            dlg.destroy()
            return

        dlg.destroy()
        configs = config.parse_config()
        index = configs["paths"].index(dirname)
        configs["paths"].pop(index)
        configs["paths"].insert(index, new_dirname)
        config.set_config(configs)
        self.view.paths_list.edit_selected_value(0, new_dirname)


    def add_var(self):
        dlg = Gtk.Dialog()
        dlg.add_button("Add", Gtk.ResponseType.ACCEPT)
        dlg.add_button("Cancel", Gtk.ResponseType.CANCEL)
        box = dlg.get_content_area()

        _box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        label = Gtk.Label(label="Variable:")
        var_entry = Gtk.Entry()
        _box.pack_start(label, False, True, 10)
        _box.pack_start(var_entry, True, True, 10)
        box.add(_box)

        _box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        label = Gtk.Label(label="Value:")
        val_entry = Gtk.Entry()
        _box.pack_start(label, False, True, 10)
        _box.pack_start(val_entry, True, True, 10)
        box.add(_box)
        box.show_all()

        response = dlg.run()
        if response == Gtk.ResponseType.CANCEL:
            dlg.destroy()
            return

        var_name = var_entry.get_text().strip()
        val = val_entry.get_text().strip()
        if var_name == "" or val == "":
            dlg.destroy()
            return
        dlg.destroy()
        self.view.vars_list.add_var([var_name, val])
        configs = config.parse_config()
        configs["vars"][var_name] = val
        config.set_config(configs)


    def remove_var(self):
        # get current selected variable
        var_name = self.view.vars_list.get_selected_value(0)

        # update config file
        configs = config.parse_config()
        del configs["vars"][var_name]
        config.set_config(configs)

        # update store
        self.view.vars_list.remove_selected()


    def edit_var(self):
        # get current selected variable
        var_name = self.view.vars_list.get_selected_value(0)

        dlg = Gtk.Dialog()
        dlg.add_button("Edit", Gtk.ResponseType.ACCEPT)
        dlg.add_button("Cancel", Gtk.ResponseType.CANCEL)
        box = dlg.get_content_area()

        _box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        label = Gtk.Label(label="Value:")
        val_entry = Gtk.Entry()
        _box.pack_start(label, False, True, 10)
        _box.pack_start(val_entry, True, True, 10)
        box.add(_box)
        box.show_all()

        response = dlg.run()

        if response == Gtk.ResponseType.CANCEL:
            dlg.destroy()
            return

        val = val_entry.get_text().strip()
        if val == "":
            dlg.destroy()
            return

        dlg.destroy()

        # set store
        self.view.vars_list.edit_selected_value(1, val)

        # update config file
        configs = config.parse_config()
        configs["vars"][var_name] = val
        config.set_config(configs)
