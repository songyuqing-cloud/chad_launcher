import os
from pathlib import Path
from typing import Optional

from gi.repository import GdkPixbuf, Gtk

from ..game import Game

IMAGE_WIDTH = 460
IMAGE_HEIGHT = 215
IMAGE_SCALE = 0.7


@Gtk.Template(
    filename=os.path.join(os.path.dirname(__file__), "../data/templates/card.glade")
)
class Card(Gtk.Button):
    __gtype_name__ = "card"
    spinner = Gtk.Template.Child()
    image = Gtk.Template.Child()
    label = Gtk.Template.Child()

    game: Game
    image_path: Optional[Path]
    title: str

    def __init__(self, game: Game, image_path: Optional[Path], title: str):
        super(Gtk.Button, self).__init__()
        self.update(game, image_path, title)
        self.setup()

    def start_loading(self):
        self.spinner.start()

    def stop_loading(self):
        self.spinner.stop()

    def update(self, game: Game, image_path: Optional[Path], title: str):
        self.set_title(title)
        self.set_image(image_path)
        self.game = game

    def set_image(self, path: Optional[Path]):
        self.image_path = path
        if path is not None:
            str_path = str(path)
        else:
            str_path = str(
                Path(__file__).parent.parent / "data" / "assets" / "default_banner.png"
            )

        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(
            str_path, width=IMAGE_WIDTH * IMAGE_SCALE, height=IMAGE_HEIGHT * IMAGE_SCALE, preserve_aspect_ratio=False
        )
        self.image.set_from_pixbuf(pixbuf)
        self.image.set_size_request(IMAGE_WIDTH * IMAGE_SCALE, IMAGE_HEIGHT * IMAGE_SCALE)

    def set_title(self, label: str):
        self.title = label
        self.label.set_text(self.title)

    def setup(self):
        self.label.set_text(self.title)
        self.set_image(self.image_path)
