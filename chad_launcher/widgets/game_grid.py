import os
from typing import Callable, List, Optional

from gi.repository import Gtk

from ..game import Game
from .card import Card


@Gtk.Template(
    filename=os.path.join(
        os.path.dirname(__file__), "../data/templates/game-grid.glade"
    )
)
class GameGrid(Gtk.Box):
    __gtype_name__ = "game-grid"
    grid: Gtk.FlowBox = Gtk.Template.Child()
    scroll_view: Gtk.ScrolledWindow = Gtk.Template.Child()

    def __init__(self):
        super().__init__()

    def remove_games(self):
        for child in self.grid.get_children():
            self.grid.remove(child)
            child.destroy()

    def create_card(self, child, game):
        card = Card(
            game,
            None,
            "",
        )

        def handle_card_click(_):
            self.grid.select_child(child)

        card.connect("clicked", handle_card_click)
        child.add(card)
        return card

    def update_games(self, games: List[Game], append: bool):
        if not append:
            for child in self.grid.get_children():
                child.set_visible(False)

        for index, game in enumerate(games):
            if self.grid.get_child_at_index(index) is not None and not append:
                child = self.grid.get_child_at_index(index)
                card = child.get_child()
                child.set_visible(True)
            else:
                child = Gtk.FlowBoxChild()
                card = self.create_card(child, game)
                self.grid.add(child)

            if game.banner_path.is_file():
                image = game.banner_path
            else:
                image = None

            card.update(game, image, game.name)
            child.set_tooltip_text(game.name)
            child.show_all()

    def register_select_game_handler(self, handler: Callable[[Game], None]):
        self.grid.connect(
            "selected-children-changed",
            lambda box: handler(box.get_selected_children()[0].get_child().game),
        )

    def register_scroll_handler(self, handler):
        self.scroll_view.get_vadjustment().connect(
            "value-changed",
            lambda adj: handler(adj.get_value(), adj.get_upper(), adj.get_page_size()),
        )
