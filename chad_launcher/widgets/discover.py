import logging
import re
from pathlib import Path
from typing import Callable, List

import gi

from ..discover_game import DiscoverGame
from .game_grid import GameGrid
from .panel import Panel

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


@Gtk.Template(
    filename=str(
        Path(__file__).parent.parent / "data" / "templates" / "discover-library.glade"
    )
)
class DiscoverLibrary(Gtk.Grid):
    __gtype_name__ = "discover"
    grid_wrapper: Gtk.Box = Gtk.Template.Child()
    panel_wrapper: Gtk.Box = Gtk.Template.Child()

    spinner: Gtk.Spinner
    button_magnet: Gtk.Button
    button_import: Gtk.Button
    button_report_bug: Gtk.Button
    button_reload: Gtk.Button

    game_grid: GameGrid
    panel: Panel

    def __init__(self):
        super(Gtk.Grid, self).__init__()
        self.sort_mode = "name"
        self.select_game_handler = None
        self.game_grid = GameGrid()

        self.spinner = Gtk.Spinner()
        self.button_magnet = Gtk.Button.new_with_label("Magnet")
        self.button_report_bug = Gtk.Button.new_with_label("Report Bug")
        self.button_import = Gtk.Button.new_with_label("Import")
        self.button_reload = Gtk.Button.new_with_label("Reload")
        self.panel = Panel(
            [
                self.button_magnet,
                self.button_report_bug,
                self.button_import,
                self.button_reload,
                self.spinner,
            ]
        )

    def setup(self):
        self.setup_grid()
        self.setup_panel()

    def setup_grid(self):
        self.grid_wrapper.add(self.game_grid)

    def setup_panel(self):
        self.panel.setup()
        self.panel_wrapper.add(self.panel)

    def set_loading(self, loading: bool):
        if loading:
            self.spinner.start()
        else:
            self.spinner.stop()

    def update_panel(self, game: DiscoverGame):
        self.panel.update_game(game)

        self.panel.set_info(game.description)

        if game.sys_reqs is not None:
            text = "System Requirements:\n\n"
            for key, value in game.sys_reqs.items():
                text += f"{key}: {value}\n"
            self.panel.set_extra_info(text)

    def update_games(self, games: List[DiscoverGame], append: bool):
        self.game_grid.update_games(games, append)

    def register_import_handler(self, handler: Callable[[], None]):
        self.button_import.connect("clicked", lambda _: handler())

    def register_magnet_handler(self, handler: Callable[[], None]):
        self.button_magnet.connect("clicked", lambda _: handler())

    def register_reload_handler(self, handler: Callable[[], None]):
        self.button_reload.connect("clicked", lambda _: handler())
