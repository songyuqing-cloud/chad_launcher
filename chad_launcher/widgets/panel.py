import os
from pathlib import Path
from typing import List

from gi.repository import Gtk

from .. import utils
from ..game import Game


@Gtk.Template(
    filename=os.path.join(os.path.dirname(__file__), "../data/templates/panel.glade")
)
class Panel(Gtk.Grid):
    __gtype_name__ = "panel"
    btn_box: Gtk.ButtonBox = Gtk.Template.Child()
    image = Gtk.Template.Child()
    title: Gtk.Label = Gtk.Template.Child()
    info1: Gtk.Label = Gtk.Template.Child()
    info2: Gtk.Label = Gtk.Template.Child()

    def __init__(self, buttons: List[Gtk.Widget]):
        super().__init__()
        self.buttons = buttons

    def setup(self):
        for button in self.buttons:
            self.btn_box.add(button)

    def set_info(self, info_text: str):
        self.info1.set_text(info_text)

    def set_extra_info(self, info_text: str):
        self.info2.set_text(info_text)

    def get_info(self) -> str:
        return self.info1.get_text()

    def get_extra_info(self) -> str:
        return self.info2.get_text()

    def update_game(self, game: Game):
        self.set_info("")
        self.set_extra_info("")

        # set image
        if game.banner_path.is_file():
            utils.set_bg_image_css(self.image, "#image", game.banner_path)
        else:
            utils.set_bg_image_css(
                self.image,
                "#image",
                Path(__file__).parent.parent / "data" / "assets" / "default_banner.png",
            )

        self.title.set_text(game.name)
