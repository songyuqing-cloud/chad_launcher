import re
from pathlib import Path
from typing import Callable

from gi.repository import Gtk

@Gtk.Template(
    filename=str(
        Path(Path(__file__).parent) / ".." / "data" / "templates" / "header.glade"
    )
)
class HeaderBar(Gtk.HeaderBar):
    __gtype_name__ = "header-bar"
    search_entry: Gtk.SearchEntry = Gtk.Template.Child()
    button_settings: Gtk.Button = Gtk.Template.Child()
    stack_switcher: Gtk.StackSwitcher = Gtk.Template.Child()

    def __init__(self):
        super().__init__()

    def register_open_settings_handler(self, handler: Callable[[], None]):
        self.button_settings.connect("clicked", lambda _: handler())

    def register_search_handler(self, handler: Callable[[str], None]):
        self.search_entry.connect(
            "activate", lambda entry: handler(entry.get_text().strip())
        )

    def register_search_changed_handler(self, handler: Callable[[str], None]):
        self.search_entry.connect(
            "search-changed", lambda entry: handler(entry.get_text().strip())
        )
