import logging
import re
from pathlib import Path
from typing import Callable, List

from gi.repository import Gtk

from ..library_game import LibraryGame
from .game_grid import GameGrid
from .panel import Panel

logger = logging.getLogger(__name__)


@Gtk.Template(
    filename=str(Path(__file__).parent.parent / "data" / "templates" / "library.glade")
)
class Library(Gtk.Grid):
    __gtype_name__ = "library"
    grid_wrapper: Gtk.Box = Gtk.Template.Child()
    panel_wrapper: Gtk.Box = Gtk.Template.Child()

    spinner: Gtk.Label
    button_launch: Gtk.Button
    button_refresh_data: Gtk.Button
    button_terminal: Gtk.Button

    query: str

    def __init__(self):
        super(Gtk.Grid, self).__init__()
        self.sort_mode = "name"
        self.updating = False
        self.game_grid = GameGrid()
        self.query = ""

        self.spinner = Gtk.Spinner()
        self.button_launch = Gtk.Button.new_with_label("Launch")
        self.button_refresh_data = Gtk.Button.new_with_label("Refresh Data")
        self.button_terminal = Gtk.Button.new_with_label("Terminal")
        self.panel = Panel(
            [
                self.button_launch,
                self.button_refresh_data,
                self.button_terminal,
                self.spinner,
            ]
        )

    def setup(self):
        self.setup_grid()
        self.setup_panel()

        # setup flowbox
        self.game_grid.grid.set_filter_func(self.filter_games)
        self.game_grid.grid.set_sort_func(self.sort_games)

    def set_query(self, query: str):
        self.query = query
        self.game_grid.grid.invalidate_filter()

    def setup_grid(self):
        self.grid_wrapper.add(self.game_grid)

    def setup_panel(self):
        self.panel.setup()
        self.panel_wrapper.add(self.panel)

    def set_loading(self, loading: bool):
        if loading:
            self.spinner.start()
        else:
            self.spinner.stop()

    def set_game_log(self, text: str):
        self.panel.set_extra_info(text)

    def append_to_game_log(self, text: str):
        text = self.panel.get_extra_info() + text
        self.panel.set_extra_info(text)

    def filter_games(self, child):
        query: str = self.query
        for x in query:
            if not x.isalnum():
                query = query.replace(x, "")
        regex = re.compile(query, re.IGNORECASE)
        return regex.search(child.get_child().game.name) is not None

    def sort_games(self, child1, child2):
        name1 = child1.get_child().game.name
        name2 = child2.get_child().game.name

        if self.sort_mode == "name":
            if name1 > name2:
                return 1
            if name1 == name2:
                return 0
            return -1

    def update_panel(self, game: LibraryGame):
        self.panel.update_game(game)

    def update_games(self, games: List[LibraryGame]):
        self.game_grid.update_games(games, append=False)

    def register_launch_handler(self, handler: Callable[[], None]):
        self.button_launch.connect("clicked", lambda _: handler())

    def register_refresh_data_handler(self, handler: Callable[[], None]):
        self.button_refresh_data.connect("clicked", lambda _: handler())

    def register_terminal_handler(self, handler: Callable[[], None]):
        self.button_terminal.connect("clicked", lambda _: handler())
