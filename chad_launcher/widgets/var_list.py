import os
from typing import Callable, List

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gtk


@Gtk.Template(
    filename=os.path.join(os.path.dirname(__file__), "../data/templates/var-list.glade")
)
class VarList(Gtk.Box):
    __gtype_name__ = "var-list"
    button_add: Gtk.Button = Gtk.Template.Child()
    button_delete: Gtk.Button = Gtk.Template.Child()
    button_edit: Gtk.Button = Gtk.Template.Child()
    tree_view: Gtk.TreeView = Gtk.Template.Child()

    store: Gtk.ListStore

    def __init__(self, columns: List[str]):
        super(Gtk.Box, self).__init__()

        self.store = Gtk.ListStore(*[str for _ in range(len(columns))])

        self.tree_view.set_model(self.store)

        for i, column in enumerate(columns):
            cell = Gtk.CellRendererText()
            col = Gtk.TreeViewColumn(column, cell, text=i)
            self.tree_view.append_column(col)

        self.tree_view.show_all()

    def edit_selected_value(self, index: int, value: str):
        selection = self.tree_view.get_selection()
        _, iter = selection.get_selected()
        self.store.set_value(iter, index, value)

    def remove_selected(self):
        selection = self.tree_view.get_selection()
        _, iter = selection.get_selected()
        self.store.remove(iter)

    def get_selected_value(self, index: int) -> str:
        selection = self.tree_view.get_selection()
        model, iter = selection.get_selected()
        return model.get_value(iter, index)

    def add_var(self, row: List[str]):
        self.store.append(row)

    def register_add_handler(self, handler):
        self.button_add.connect("clicked", lambda _: handler())

    def register_delete_handler(self, handler):
        self.button_delete.connect("clicked", lambda _: handler())

    def register_edit_handler(self, handler):
        self.button_edit.connect("clicked", lambda _: handler())
