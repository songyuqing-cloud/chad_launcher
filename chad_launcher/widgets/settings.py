from pathlib import Path
import os
from .var_list import VarList
from gi.repository import GLib, Gtk

from .. import config

@Gtk.Template(
    filename=os.path.join(os.path.dirname(__file__), "../data/templates/settings.glade")
)
class Settings(Gtk.Window):
    __gtype_name__ = "settings"
    paths_wrapper: Gtk.Box = Gtk.Template.Child()
    vars_wrapper: Gtk.Box = Gtk.Template.Child()

    paths_list: VarList
    vars_list: VarList

    def __init__(self):
        super(Gtk.Window, self).__init__()
        self.paths_list = VarList(["Path"])
        self.vars_list = VarList(["Variable", "Value"])

        configs = config.parse_config()

        for path in configs["paths"]:
            self.paths_list.add_var([str(path)])

        for key, value in configs["vars"].items():
            self.vars_list.add_var([key, value])

        self.paths_wrapper.add(self.paths_list)
        self.vars_wrapper.add(self.vars_list)


