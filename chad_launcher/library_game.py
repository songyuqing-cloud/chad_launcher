import asyncio
import logging
import subprocess
from datetime import datetime
from os import path
from pathlib import Path

import aiofiles
import yaml
from fuzzywuzzy import process

from .game import Game

logger = logging.getLogger(__name__)


BANNER_LIST_URL = (
    "https://gitlab.com/chad-productions/chad_launcher_banners/-/raw/master/list.txt"
)
# TODO: (blackCauldron7): Add docstrings
class LibraryGame(Game):
    def __init__(self, root_path: Path):
        self.root_path = root_path
        self.banner_path = self.root_path / "banner.png"
        self.config_path = self.root_path / "game.yaml"
        self.environ: dict = {}

    async def setup(self, session, force=False):
        await asyncio.gather(
            self.get_info(force=force), self.get_banner(session, force=force)
        )

    async def get_info(self, force=False):
        if not self.config_path.is_file() or force:
            name: str = self.root_path.name
            name = name.replace(".", " ")
            name = name.replace("_", " ")
            name = name.replace("-", " ")
            name = name.title().strip()
            self.name = name
            async with aiofiles.open(self.config_path, mode="w") as f:
                data = {"name": self.name}
                dump = yaml.dump(data, Dumper=yaml.SafeDumper)
                await f.write(dump)

        async with aiofiles.open(self.config_path, mode="r") as f:
            data = yaml.load(await f.read(), Loader=yaml.SafeLoader)
            self.name = data["name"]

    async def get_banner(self, session, force=False):
        # get game image
        if not self.banner_path.is_file() or force:
            # get banner image
            banner_list = Path("~/.cache/chad_launcher/list.txt").expanduser()
            if not banner_list.parent.is_dir():
                banner_list.parent.mkdir()

            if not banner_list.is_file() or force:
                async with session.get(BANNER_LIST_URL) as resp:
                    list = await resp.read()
                    list = list.decode()

                banner_list.write_text(datetime.today().strftime("%d-%m-%Y\n") + list)
            else:
                async with aiofiles.open(banner_list, mode="r+") as f:
                    content = await f.read()
                    content = content.strip().split("\n")
                    if len(content):
                        list_date = datetime.strptime(content[0], "%d-%m-%Y").date()
                        if datetime.today().date() > list_date:
                            async with session.get(BANNER_LIST_URL) as resp:
                                list = await resp.read()
                                await f.write(datetime.today().strftime("%d-%m-%Y\n"))
                                await f.write(list.decode())
            banners = ()
            async with aiofiles.open(banner_list, mode="r") as f:
                banners = await f.read()
                banners = banners.split("\n")[1:]
                _banners: dict = {}
                for banner in banners:
                    if ":" in banner:
                        banner = banner.split(":")
                        _banners[banner[0].strip()] = banner[1].strip()
                    else:
                        _banners[banner.strip()] = banner.strip()
                banner_name = process.extractOne(self.name, _banners.keys())

                if banner_name is None:
                    return

                if banner_name[1] < 90:
                    return

                banner_name = _banners[banner_name[0]]

            banner_url = f"https://gitlab.com/chad-productions/chad_launcher_banners/-/raw/master/{banner_name}.png?inline=false"
            # get image.
            async with session.get(banner_url) as resp:
                async with aiofiles.open(self.banner_path, mode="wb") as f:
                    await f.write(await resp.read())
        logger.debug(f"fetched banner for {self.root_path}")

    def launch_game(self) -> subprocess.Popen:
        logger.info(f"Launching {self.name}")
        proc = subprocess.Popen(
            [
                path.join(self.root_path, "start.sh"),
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            text=True,
            cwd=path.join(self.root_path),
        )
        return proc
