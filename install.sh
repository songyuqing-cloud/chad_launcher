#!/bin/sh
uid=$(id -u)

if [ $uid -eq 0 ]; then
	pip install .
	cp icon.svg /usr/share/pixmaps/chad_launcher.svg
	cp chad_launcher.desktop /usr/share/applications/chad_launcher.desktop
else
	pip install .
	cp icon.svg $HOME/.local/share/icons/chad_launcher.svg
	cp chad_launcher.desktop $HOME/.local/share/applications/chad_launcher.desktop
fi
