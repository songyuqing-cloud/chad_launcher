#!/bin/sh
uid=$(id -u)

if [ $uid -eq 0 ]; then
	pip uninstall chad-launcher
	rm /usr/share/pixmaps/chad_launcher.svg
	rm /usr/share/applications/chad_launcher.desktop
else
	pip uninstall chad-launcher
	rm $HOME/.local/share/icons/chad_launcher.svg
	rm $HOME/.local/share/applications/chad_launcher.desktop
fi
